set search_path to simui;
SET datestyle = "ISO, DMY";
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (1,21,'interface','15/3/2018',To_timestamp('17:36','hh24:mi'),84,111819.70,'PSJ','Privat','In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (2,18,'explicit','12/3/2018',To_timestamp('17:36','hh24:mi'),50,220042.95,'Auditorium Psikologi','Umum','Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',4);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (3,30,'zero administration','11/8/2018',To_timestamp('17:36','hh24:mi'),53,NULL,'Auditorium Vokasi','Privat','Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',7);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (4,30,'Robust','9/4/2018',To_timestamp('17:36','hh24:mi'),52,84161.08,'Aula Terapung','Privat','Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.',8);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (5,3,'intranet','12/7/2018',To_timestamp('17:36','hh24:mi'),34,122080.11,'Auditorium FIB','Umum','Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (6,50,'zero defect','19/1/2018',To_timestamp('17:36','hh24:mi'),41,140247.61,'Auditorium Psikologi','Umum','Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (7,47,'protocol','12/12/2017',To_timestamp('17:36','hh24:mi'),78,230746.67,'Auditorium FEB','Umum','Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (8,7,'user-facing','31/1/2018',To_timestamp('17:36','hh24:mi'),37,NULL,'Auditorium FISIP','Umum','Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (9,6,'Total','8/12/2017',To_timestamp('17:36','hh24:mi'),70,NULL,'Aula Terapung','Privat','Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (10,31,'Cross-group','17/5/2018',To_timestamp('17:36','hh24:mi'),73,168034.19,'Auditorium FISIP','Privat','Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (11,15,'empowering','15/5/2018',To_timestamp('17:36','hh24:mi'),50,102654.24,'Auditorium Vokasi','Umum','Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (12,3,'Synchronised','15/8/2018',To_timestamp('17:36','hh24:mi'),61,89494.97,'Auditorium FT','Umum','In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.',8);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (13,22,'Graphical User Interface','19/8/2018',To_timestamp('17:36','hh24:mi'),45,208131.83,'Auditorium Fasilkom','Umum','Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (14,22,'analyzer','20/5/2018',To_timestamp('17:36','hh24:mi'),39,191054.14,'Balai Sidang','Umum','Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (15,7,'exuding','13/1/2018',To_timestamp('17:36','hh24:mi'),87,89037.28,'Auditorium Fasilkom','Umum','Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (16,20,'Multi-tiered','2/12/2017',To_timestamp('17:36','hh24:mi'),59,237743.45,'Balai Sidang','Umum','Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (17,14,'Automated','5/11/2018',To_timestamp('17:36','hh24:mi'),46,187916.53,'Parkiran FISIP','Umum','Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',6);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (18,17,'knowledge base','15/10/2018',To_timestamp('17:36','hh24:mi'),100,NULL,'Balai Sidang','Umum','Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.

Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',6);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (19,4,'Progressive','18/4/2018',To_timestamp('17:36','hh24:mi'),69,89739.22,'Auditorium Vokasi','Privat','Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',6);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (20,7,'model','26/8/2018',To_timestamp('17:36','hh24:mi'),31,256131.76,'Parkiran Psikologi','Umum','Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (21,19,'monitoring','7/10/2018',To_timestamp('17:36','hh24:mi'),49,223952.26,'Auditorium FEB','Umum','In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (22,2,'Optional','10/11/2018',To_timestamp('17:36','hh24:mi'),33,NULL,'Auditorium FIB','Privat','Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (23,4,'Pre-emptive','3/7/2018',To_timestamp('17:36','hh24:mi'),30,72639.58,'Auditorium FEB','Umum','Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (24,42,'matrix','3/3/2018',To_timestamp('17:36','hh24:mi'),76,NULL,'Auditorium FIB','Umum','In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.',7);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (25,29,'instruction set','19/3/2018',To_timestamp('17:36','hh24:mi'),48,NULL,'Parkiran Psikologi','Privat','Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (26,43,'holistic','16/12/2017',To_timestamp('17:36','hh24:mi'),71,116576.48,'Parkiran Psikologi','Umum','Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.',5);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (27,31,'Switchable','8/12/2017',To_timestamp('17:36','hh24:mi'),32,NULL,'Auditorium Fasilkom','Umum','Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (28,31,'Persevering','17/2/2018',To_timestamp('17:36','hh24:mi'),67,106834.09,'Auditorium FT','Privat','Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (29,2,'zero administration','1/8/2018',To_timestamp('17:36','hh24:mi'),69,NULL,'Parkiran Psikologi','Privat','Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',4);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (30,9,'collaboration','28/8/2018',To_timestamp('17:36','hh24:mi'),95,NULL,'Auditorium Psikologi','Umum','Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.',8);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (31,49,'Triple-buffered','2/6/2018',To_timestamp('17:36','hh24:mi'),69,NULL,'PSJ','Umum','Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (32,48,'client-server','3/5/2018',To_timestamp('17:36','hh24:mi'),37,264737.22,'Auditorium Psikologi','Privat','In congue. Etiam justo. Etiam pretium iaculis justo.',7);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (33,26,'Inverse','23/1/2018',To_timestamp('17:36','hh24:mi'),98,85979.81,'Balairung','Privat','Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.',8);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (34,39,'installation','5/3/2018',To_timestamp('17:36','hh24:mi'),52,150314.85,'Balairung','Umum','Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (35,9,'Robust','19/7/2018',To_timestamp('17:36','hh24:mi'),68,96433.88,'Auditorium FISIP','Privat','Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (36,40,'Monitored','18/9/2018',To_timestamp('17:36','hh24:mi'),86,189552.19,'Aula Terapung','Privat','Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (37,19,'pricing structure','27/3/2018',To_timestamp('17:36','hh24:mi'),41,NULL,'Auditorium Vokasi','Umum','Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (38,16,'Ergonomic','31/8/2018',To_timestamp('17:36','hh24:mi'),48,187524.96,'Balai Sidang','Umum','Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (39,10,'Enhanced','9/2/2018',To_timestamp('17:36','hh24:mi'),70,NULL,'Parkiran FISIP','Umum','Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (40,27,'non-volatile','24/4/2018',To_timestamp('17:36','hh24:mi'),43,NULL,'Auditorium Vokasi','Umum','In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.',5);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (41,1,'Fundamental','24/5/2018',To_timestamp('17:36','hh24:mi'),41,83213.75,'Auditorium FT','Privat','Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (42,44,'analyzing','6/3/2018',To_timestamp('17:36','hh24:mi'),89,78439.17,'Auditorium FEB','Umum','Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (43,27,'systematic','27/1/2018',To_timestamp('17:36','hh24:mi'),31,236894.74,'Auditorium FISIP','Umum','Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',4);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (44,1,'impactful','15/4/2018',To_timestamp('17:36','hh24:mi'),83,131576.00,'Auditorium Psikologi','Umum','Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (45,5,'interface','6/7/2018',To_timestamp('17:36','hh24:mi'),97,196716.17,'Auditorium FISIP','Umum','Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',5);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (46,36,'User-friendly','16/7/2018',To_timestamp('17:36','hh24:mi'),51,63309.74,'Auditorium Fasilkom','Umum','Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (47,26,'Multi-tiered','17/8/2018',To_timestamp('17:36','hh24:mi'),52,287649.54,'PSJ','Umum','Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (48,36,'infrastructure','28/3/2018',To_timestamp('17:36','hh24:mi'),34,NULL,'PSJ','Umum','Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (49,5,'contingency','20/2/2018',To_timestamp('17:36','hh24:mi'),80,148788.73,'Auditorium Vokasi','Umum','Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (50,43,'project','4/5/2018',To_timestamp('17:36','hh24:mi'),54,NULL,'Auditorium FT','Privat','Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (51,47,'product','13/1/2018',To_timestamp('17:36','hh24:mi'),71,NULL,'PSJ','Privat','Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (52,13,'Front-line','5/12/2017',To_timestamp('17:36','hh24:mi'),97,71435.89,'Aula Terapung','Privat','Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.',6);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (53,44,'local','19/4/2018',To_timestamp('17:36','hh24:mi'),94,NULL,'Auditorium Vokasi','Umum','Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (54,47,'Secured','13/12/2017',To_timestamp('17:36','hh24:mi'),31,51159.18,'Auditorium FEB','Umum','Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (55,47,'clear-thinking','22/9/2018',To_timestamp('17:36','hh24:mi'),50,191996.44,'Auditorium Psikologi','Umum','In congue. Etiam justo. Etiam pretium iaculis justo.',6);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (56,7,'background','5/4/2018',To_timestamp('17:36','hh24:mi'),54,65300.74,'Parkiran FISIP','Privat','Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.

Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.',5);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (57,15,'Team-oriented','9/2/2018',To_timestamp('17:36','hh24:mi'),86,238590.21,'Auditorium Fasilkom','Privat','Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.

Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.',6);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (58,14,'asynchronous','18/5/2018',To_timestamp('17:36','hh24:mi'),33,NULL,'Auditorium FEB','Umum','Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (59,17,'Cloned','5/7/2018',To_timestamp('17:36','hh24:mi'),88,63795.01,'Auditorium Fasilkom','Umum','Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.',4);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (60,45,'cohesive','23/5/2018',To_timestamp('17:36','hh24:mi'),33,278770.09,'Balai Sidang','Umum','In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.',8);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (61,13,'asynchronous','17/12/2017',To_timestamp('17:36','hh24:mi'),77,91285.82,'Parkiran FISIP','Umum','Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (62,19,'definition','2/9/2018',To_timestamp('17:36','hh24:mi'),96,NULL,'Auditorium Fasilkom','Privat','Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',5);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (63,6,'Graphical User Interface','29/5/2018',To_timestamp('17:36','hh24:mi'),78,281949.36,'Audiorium FH','Privat','Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.',8);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (64,45,'Cross-group','4/3/2018',To_timestamp('17:36','hh24:mi'),60,NULL,'Parkiran Psikologi','Privat','Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (65,12,'executive','13/4/2018',To_timestamp('17:36','hh24:mi'),34,NULL,'Balairung','Privat','Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.',7);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (66,16,'methodology','17/4/2018',To_timestamp('17:36','hh24:mi'),47,295564.51,'Auditorium FEB','Umum','Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',6);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (67,29,'info-mediaries','19/5/2018',To_timestamp('17:36','hh24:mi'),81,NULL,'Balairung','Umum','Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.',2);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (68,21,'analyzer','23/10/2018',To_timestamp('17:36','hh24:mi'),50,NULL,'Auditorium FIB','Umum','Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (69,3,'access','3/9/2018',To_timestamp('17:36','hh24:mi'),98,NULL,'Aula Terapung','Privat','Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.

In congue. Etiam justo. Etiam pretium iaculis justo.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (70,34,'Exclusive','9/3/2018',To_timestamp('17:36','hh24:mi'),80,182606.95,'Audiorium FH','Privat','Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (71,18,'regional','5/9/2018',To_timestamp('17:36','hh24:mi'),89,255961.01,'Auditorium Vokasi','Privat','Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (72,8,'bifurcated','6/10/2018',To_timestamp('17:36','hh24:mi'),82,NULL,'Auditorium FT','Umum','Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.

Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.',10);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (73,20,'transitional','3/2/2018',To_timestamp('17:36','hh24:mi'),58,208100.27,'Balai Sidang','Umum','Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (74,2,'Sharable','10/4/2018',To_timestamp('17:36','hh24:mi'),67,224651.73,'Parkiran Psikologi','Umum','Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.',5);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (75,40,'local area network','2/7/2018',To_timestamp('17:36','hh24:mi'),40,NULL,'Auditorium FEB','Privat','Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.',9);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (76,8,'Function-based','23/10/2018',To_timestamp('17:36','hh24:mi'),62,111347.11,'Auditorium Psikologi','Privat','Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.',3);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (77,7,'Streamlined','20/2/2018',To_timestamp('17:36','hh24:mi'),48,153008.90,'Balai Sidang','Privat','Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (78,44,'dynamic','30/7/2018',To_timestamp('17:36','hh24:mi'),54,122671.91,'Balai Sidang','Privat','Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.',7);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (79,24,'Progressive','18/4/2018',To_timestamp('17:36','hh24:mi'),47,226859.22,'Aula Terapung','Privat','Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.',1);
INSERT INTO event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori) VALUES (80,18,'tertiary','30/11/2017',To_timestamp('17:36','hh24:mi'),31,123235.23,'Audiorium FH','Privat','Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.',5);
