from django.conf.urls import url
from .views import index

from .views import *
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^list_organisasi/',list_organisasi, name='list_organisasi'),
    url(r'^create_organisasi/',create_organisasi, name='create_organisasi'),
    url(r'^update_organisasi/(?P<id>[\w|\W]+)/$',update_organisasi, name='update_organisasi'),
    url(r'^list_kepanitiaan/',list_kepanitiaan, name='list_kepanitiaan'),
    url(r'^create_kepanitiaan/',create_kepanitiaan, name='create_kepanitiaan'),
    url(r'^update_kepanitiaan/(?P<id>[\w|\W]+)/$',update_kepanitiaan, name='update_kepanitiaan'),
    url(r'^list_event/',list_event, name='list_event'),
    url(r'^create_event/',create_event, name='create_event'),
    url(r'^update_event/(?P<id>[\w|\W]+)/$',update_event, name='update_event'),
    url(r'^show_login/',show_login, name='show_login'),
    url(r'^login/', login, name='login'),
    url(r'^logout/', logout, name='logout'),
    url(r'^delete_organisasi/(?P<id>[\w|\W]+)/$',delete_organisasi, name='delete_organisasi'),
    url(r'^delete_kepanitiaan/(?P<id>[\w|\W]+)/$',delete_kepanitiaan, name='delete_kepanitiaan'),
    url(r'^delete_event/(?P<id>[\w|\W]+)/$',delete_event, name='delete_event'),
    url(r'^add_org/', add_org, name='add_org'),
    url(r'^update_org/', update_org, name='update_org'),
    url(r'^add_kepanitiaan/', add_kepanitiaan, name='add_kepanitiaan'),
    url(r'^update_panit/', update_panit, name='update_panit'),
    url(r'^add_event/', add_event, name='add_event'),
    url(r'^update_ev/', update_ev, name='update_event'),
]
