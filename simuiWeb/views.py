from django.shortcuts import render
from django.db import connection

from django.urls import reverse

from django.http import Http404

import http.client
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect

import random
import string
import datetime
from random import randint

response = {}
def index(request):
	cursor = connection.cursor()
	cursor.execute("SELECT nama from SIMUI.PEMBUAT_EVENT, SIMUI.ORGANISASI where id = id_organisasi LIMIT 5")
	list_organisasi = []
	cursor_all = cursor.fetchall()
	if len(cursor_all) > 0:
		columns = [name[0] for name in cursor.description]
		for x in cursor_all:
			list_organisasi.append(dict(zip(columns, x)))
	response['list_organisasi'] = list_organisasi
	cursor.execute("SELECT nama from SIMUI.PEMBUAT_EVENT, SIMUI.KEPANITIAAN where id = id_kepanitiaan LIMIT 5")
	list_kepanitiaan = []
	cursor_all = cursor.fetchall()
	if len(cursor_all) > 0:
		columns = [name[0] for name in cursor.description]
		for x in cursor_all:
			list_kepanitiaan.append(dict(zip(columns, x)))
	response['list_kepanitiaan'] = list_kepanitiaan
	cursor.execute("SELECT nama from SIMUI.PEMBUAT_EVENT LIMIT 5")
	list_event = []
	cursor_all = cursor.fetchall()
	if len(cursor_all) > 0:
		columns = [name[0] for name in cursor.description]
		for x in cursor_all:
			list_event.append(dict(zip(columns, x)))
	response['list_event'] = list_event
	return render(request, 'LandingPage.html', response)
def show_login(request):
	response['message'] = "yes"
	if ("role" in request.session):
		return HttpResponseRedirect(reverse('core:index'))
	return render(request, 'login.html', response)
def list_organisasi(request):
	cursor = connection.cursor()
	cursor.execute("SELECT * from SIMUI.PEMBUAT_EVENT, SIMUI.ORGANISASI where id = id_organisasi")
	list_organisasi = []
	cursor_all = cursor.fetchall()
	if len(cursor_all) > 0:
		columns = [name[0] for name in cursor.description]
		for x in cursor_all:
			list_organisasi.append(dict(zip(columns, x)))

	response['list'] = list_organisasi
	return render(request,'ListOrganisasi.html', response)
def update_organisasi(request, id):
	request.session['id'] = id
	cursor = connection.cursor()
	cursor.execute("SELECT nama,email,alamat_website,tingkatan,kategori,logo,deskripsi,contact_person from SIMUI.PEMBUAT_EVENT where id = '"+id+"'")
	select = cursor.fetchone()
	response['nama'] = select[0]
	response['email'] = select[1]
	response['website'] = select[2]
	response['tingkatan'] = select[3]
	response['kategori'] = select[4]
	response['logo'] = select[5]
	response['deskripsi'] = select[6]
	response['cp'] = select[7]
	return render(request,'OrganisasiUpdate.html', response)
def create_organisasi(request):
	return render(request,'OrganisasiFill.html', response)
def delete_organisasi(request, id):
	theidInt = int(id)
	theid = str(theidInt - 1)
	print(theid)
	cursor=connection.cursor()
	cursor.execute("DELETE from simui.organisasi where id_organisasi = '"+id+"'")
	cursor.execute("DELETE from simui.pembuat_event where id = '"+id+"'")
	return HttpResponseRedirect(reverse('core:list_organisasi'))
def list_kepanitiaan(request):
    cursor = connection.cursor()
    cursor.execute("SELECT k.id, k.nama as kepanitiaan, o.nama as organisasi,k.email,k.alamat_website,k.tingkatan,k.kategori,k.logo,k.deskripsi,k.contact_person from SIMUI.pembuat_event k, SIMUI.kepanitiaan , SIMUI.pembuat_event o where k.id = id_kepanitiaan and o.id = id_organisasi")
    list_kepanitiaan = []
    cursor_all = cursor.fetchall()
    if len(cursor_all) > 0:
        columns = [name[0] for name in cursor.description]
        for x in cursor_all:
            list_kepanitiaan.append(dict(zip(columns, x)))
    print(list_kepanitiaan)
    response['list'] = list_kepanitiaan
    return render(request,'ListKepanitiaan.html', response)
def update_kepanitiaan(request, id):
	request.session['id'] = id
	cursor = connection.cursor()
	cursor.execute("SELECT nama,email,alamat_website,tingkatan,kategori,logo,deskripsi,contact_person from SIMUI.PEMBUAT_EVENT where id = '"+id+"'")
	select = cursor.fetchone()
	response['nama'] = select[0]
	response['email'] = select[1]
	response['website'] = select[2]
	response['tingkatan'] = select[3]
	response['kategori'] = select[4]
	response['logo'] = select[5]
	response['deskripsi'] = select[6]
	response['cp'] = select[7]
	return render(request,'KepanitiaanUpdate.html', response)
def create_kepanitiaan(request):
	return render(request,'Kepanitiaanfill.html', response)
def delete_kepanitiaan(request, id):
	theidInt = int(id)
	theid = str(theidInt - 1)
	print(theid)
	cursor=connection.cursor()
	cursor.execute("DELETE from simui.kepanitiaan where id_kepanitiaan = '"+id+"'")
	cursor.execute("DELETE from simui.pembuat_event where id = '"+id+"'")
	return render(request,'ListKepanitiaan.html', response)
def list_event(request):
    cursor = connection.cursor()
    cursor.execute("SELECT id_event,id_pembuat_event,nama,tanggal,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori,poster from SIMUI.EVENT")
    list_event = []
    cursor_all = cursor.fetchall()
    if len(cursor_all) > 0:
        columns = [name[0] for name in cursor.description]
        for x in cursor_all:
            list_event.append(dict(zip(columns, x)))
    response['list'] = list_event
    return render(request,'ListEvent.html', response)
def update_event(request, id):
	request.session['id'] = id
	cursor = connection.cursor()
	cursor.execute("SELECT nama,tanggal,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori,poster from SIMUI.EVENT where id_event = '"+id+"'")
	select = cursor.fetchone()
	response['nama'] = select[0]
	response['tanggal'] = select[1]
	response['kapasitas'] = select[2]
	response['tiket'] = select[3]
	response['lokasi'] = select[4]
	response['sifat'] = select[5]
	response['deskripsi'] = select[6]
	response['kategori'] = select[7]
	response['poster'] = select[8]
	return render(request,'EventUpdate.html', response)
def create_event(request):
		return render(request,'EventFill.html', response)
def delete_event(request, id):
	theidInt = int(id)
	theid = str(theidInt - 1)
	print(theid)
	cursor=connection.cursor()
	cursor.execute("DELETE from simui.event where id_event = '"+id+"'")
	return render(request,'ListOrganisasi.html', response)
@csrf_exempt
def login(request):
	if(request.method == "POST"):
		username = request.POST['Username']
		password = request.POST['Password']
		cursor=connection.cursor()
		cursor.execute("SELECT * from SIMUI.PENGGUNA where username='"+username+"'")
		select=cursor.fetchone()
		if(select):
			print("user detect")
			cursor.execute("SELECT password from SIMUI.PENGGUNA where username='"+username+"'")
			select=cursor.fetchone()
			if select[0] == password:
				print("password detect")
				cursor.execute("SELECT * from SIMUI.STAFF where username='"+username+"'")
				select=cursor.fetchone()
				if(select):
					print("halo2")
					response['message'] = "yes"
					request.session['role'] = "humas"
					return HttpResponseRedirect(reverse('core:index'))
				cursor.execute("SELECT * from SIMUI.ADMIN where username='"+username+"'")
				select=cursor.fetchone()
				if(select):
					print("halo")
					response['message'] = "yes"
					request.session['role'] = "admin"
					return HttpResponseRedirect(reverse('core:index'))
				response['message'] = "yes"
				request.session['role'] = "non_admin"
				return HttpResponseRedirect(reverse('core:index'))
			else:
				response['message'] = "no"
				return render(request, 'login.html', response)
		else:
			response['message'] = "no"
			return render(request, 'login.html', response)
	response['message'] = "no"
	return render(request, 'login.html', response)
def logout(request):
		request.session.clear()
		return HttpResponseRedirect(reverse('core:index'))

@csrf_exempt
def add_org(request):
	if request.method == 'POST':
		cursor = connection.cursor()
		intg=randint(51, 99999)
		theid = request.session.get('theid',intg)
		theidStr = str(theid)
		print(theidStr)
		cursor.execute("SELECT id from SIMUI.PEMBUAT_EVENT where id = '"+theidStr+"'")
		select = cursor.fetchone()
		print(select)
		if(select):
			while(select[0] == intg):
				intg=randint(51, 99999)
				theid = request.session.get('theid',intg)

		print("id")
		print(theid)
		theidStr = str(theid)
		name = request.POST['name']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		cp = request.POST['cp']
		kategori = request.POST['kategori']
		logo = request.POST['logo']
		deskripsi = request.POST['des']

		cursor.execute("INSERT INTO simui.pembuat_event(id,nama,email,alamat_website,tingkatan,kategori,logo,deskripsi,contact_person) VALUES ('"+theidStr+"','"+name+"','"+email+"','"+website+"','"+tingkatan+"','"+kategori+"', '"+logo+"','"+deskripsi+"','"+cp+"')")
		cursor.execute("INSERT INTO simui.organisasi(id_organisasi) VALUES ('"+theidStr+"')")
		theidInt = int(theid)
		theidInt += 1
		request.session['theid'] = theidInt
	return HttpResponseRedirect(reverse('core:list_organisasi'))

@csrf_exempt
def update_org(request):
	print("masuk")
	if request.method == 'POST':
		cursor = connection.cursor()
		theid = request.session.get('id')
		name = request.POST['name']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		cp = request.POST['cp']
		kategori = request.POST['kategori']
		logo = request.POST['logo']
		deskripsi = request.POST['des']
		cursor.execute("UPDATE SIMUI.PEMBUAT_EVENT SET nama = '"+name+"' , email = '"+email+"', alamat_website = '"+website+"', tingkatan = '"+tingkatan+"', kategori = '"+kategori+"', logo = '"+logo+"', deskripsi = '"+deskripsi+"', contact_person = '"+cp+"' where id = '"+theid+"' ")
	return HttpResponseRedirect(reverse('core:list_organisasi'))

@csrf_exempt
def add_kepanitiaan(request):
	if request.method == 'POST':
		cursor=connection.cursor()
		intg=randint(51, 99999)
		theid = request.session.get('theid',intg)
		theidStr = str(theid)
		print(theidStr)
		cursor.execute("SELECT id from SIMUI.PEMBUAT_EVENT where id = '"+theidStr+"'")
		select = cursor.fetchone()
		print(select)
		if(select):
			while(select[0] == intg):
				intg=randint(51, 99999)
				theid = request.session.get('theid',intg)

		print("id")
		print(theid)
		theidStr = str(theid)



		name = request.POST['nama']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		cp = request.POST['cp']
		kategori = request.POST['kategori']
		logo = request.POST['pic']
		deskripsi = request.POST['deskripsi']

		cursor.execute("INSERT INTO simui.pembuat_event(id,nama,email,alamat_website,tingkatan,kategori,logo,deskripsi,contact_person) VALUES ('"+theidStr+"','"+name+"','"+email+"','"+website+"','"+tingkatan+"','"+kategori+"', '"+logo+"','"+deskripsi+"','"+cp+"')")
		cursor.execute("INSERT INTO simui.kepanitiaan(id_kepanitiaan, id_organisasi) VALUES ('"+theidStr+"', 1)")
		theidInt = int(theid)
		theidInt += 1
		request.session['theid'] = theidInt
	return HttpResponseRedirect(reverse('core:list_kepanitiaan'))	


@csrf_exempt
def update_panit(request):
	print("masuk")
	if request.method == 'POST':
		cursor = connection.cursor()
		theid = request.session.get('id')
		name = request.POST['nama']
		tingkatan = request.POST['tingkatan']
		email = request.POST['email']
		website = request.POST['website']
		cp = request.POST['cp']
		kategori = request.POST['kategori']
		logo = request.POST['pic']
		deskripsi = request.POST['deskripsi']
		cursor.execute("UPDATE SIMUI.PEMBUAT_EVENT SET nama = '"+name+"' , email = '"+email+"', alamat_website = '"+website+"', tingkatan = '"+tingkatan+"', kategori = '"+kategori+"', logo = '"+logo+"', deskripsi = '"+deskripsi+"', contact_person = '"+cp+"' where id = '"+theid+"' ")
	return HttpResponseRedirect(reverse('core:list_kepanitiaan'))

@csrf_exempt
def add_event(request):
	if request.method == 'POST':
		cursor=connection.cursor()
		intg=randint(51, 99999)
		theid = request.session.get('theid',intg)
		theidStr = str(theid)
		print(theidStr)
		cursor.execute("SELECT id_event from SIMUI.EVENT where id_event = '"+theidStr+"'")
		select = cursor.fetchone()
		print(select)
		if(select):
			while(select[0] == intg):
				intg=randint(51, 99999)
				theid = request.session.get('theid',intg)

		print("id")
		print(theid)
		theidStr = str(theid)


		name = request.POST['nama']
		tanggal = request.POST['tanggal']
		kapasitas = request.POST['kapasitas']
		harga_tiket = request.POST['tiket']
		lokasi = request.POST['lokasi']
		kategori = str(request.POST['kategori'])
		sifat = request.POST['sifat']
		poster = request.POST['poster']
		deskripsi = request.POST['deskripsi']
		cursor.execute("INSERT INTO simui.event(id_event,id_pembuat_event,nama,tanggal,waktu,kapasitas,harga_tiket,lokasi,sifat_event,deskripsi_singkat,nomor_kategori,poster) VALUES ('"+theidStr+"', 1 ,'"+name+"','"+tanggal+"', To_timestamp('17:36','hh24:mi') ,'"+kapasitas+"','"+harga_tiket+"', '"+lokasi+"','"+sifat+"','"+deskripsi+"','"+kategori+"','"+poster+"')")
		theidInt = int(theid)
		theidInt += 1
		request.session['theid'] = theidInt
	return HttpResponseRedirect(reverse('core:list_event'))

@csrf_exempt
def update_ev(request):
	print("masuk")
	if request.method == 'POST':
		cursor = connection.cursor()
		theid = request.session.get('id')
		name = request.POST['nama']
		tanggal = request.POST['tanggal']
		kapasitas = request.POST['kapasitas']
		tiket = request.POST['tiket']
		lokasi = request.POST['lokasi']
		kategori = str(request.POST['kategori'])
		sifat = request.POST['sifat']
		poster = request.POST['poster']
		deskripsi = request.POST['des']
		cursor.execute("UPDATE SIMUI.EVENT SET nama = '"+name+"' , tanggal = '"+tanggal+"', kapasitas = '"+kapasitas+"', harga_tiket = '"+tiket+"', lokasi = '"+lokasi+"', sifat_event = '"+sifat+"', deskripsi_singkat = '"+deskripsi+"', nomor_kategori = '"+kategori+"', poster = '"+poster+"' where id_event = '"+theid+"' ")
	return HttpResponseRedirect(reverse('core:list_event'))